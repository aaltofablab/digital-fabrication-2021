# Fab Academy 2021

This is the Digital Fabrication I, II, III and Studio course repository of the Aalto Fablab. It links to individual participants GitLab repositories and serves as a workspace to work on group assignments.

This repository is being automatically published each time one `git push`-es to it. How it happens is described in the `gitlab-ci.yml` file.

[Click here](https://aaltofablab.gitlab.io/digital-fabrication-2021/) to access website.
