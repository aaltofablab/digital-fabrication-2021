+++
title = "Electronics Production"
+++

Page for Electronics Production group assignment. 

- Characterize the design rules for your PCB production process: document feeds, speeds, plunge rate, depth of cut (traces and outline) and tooling.
- Document your work (in a group or individually).

### Group A

- Daniel Hopkins
- Hannu ikola
- Karl Mihhels

### Group B

- Onni Eriksson
- Eero Pitkänen
- Ekaterina Voskoboinik
- Dinan Yan

### Group C

- Dann Mensah
- Daniel Wilenius
- Ruo-Xuan Wu

{{<button href="https://mensahdann.gitlab.io/fab-academy/assignments/a4g/">}}View docs{{</button>}}

### Group D

- Solomon Embafrash
- Ranjit Menon
- Kitija Kuduma
