+++
title = "3D Scanning and Printing"
+++

Page for 3D Scanning and Printing group assignment.

- Test the design rules for your printer(s).
- Document your work and explain what are the limits of your printer(s) (in a group or individually).

### Group A

- Daniel Hopkins
- Hannu ikola
- Karl Mihhels

### Group B

- Onni Eriksson
- Eero Pitkänen
- Ekaterina Voskoboinik
- Dinan Yan

### Group C

- Dann Mensah
- Daniel Wilenius
- Ruo-Xuan Wu

{{<button href="https://mensahdann.gitlab.io/fab-academy/assignments/a4g/">}}View docs{{</button>}}

### Group D

- Solomon Embafrash
- Ranjit Menon
- Kitija Kuduma
