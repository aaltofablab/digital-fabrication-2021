+++
title = "Computer-Controlled Cutting"
+++

Computer-Controlled Cutting group assignment. Please divide responsibilities:

- One of you should do the machine work.
- One of you should take pictures and videos.
- One of you should add it to the documentation page of the computer-controlled cutting week.
- All of you should link to the documentation page from your website.

### Group A

- Daniel Hopkins
- Hannu ikola
- Karl Mihhels

### Group B

- Onni Eriksson
- Eero Pitkänen
- Ekaterina Voskoboinik
- Dinan Yan

### Group C

- Dann Mensah
- Daniel Wilenius
- Ruo-Xuan Wu

{{<button href="https://mensahdann.gitlab.io/fab-academy/assignments/a4g/">}}View docs{{</button>}}

### Group D

- Solomon Embafrash
- Ranjit Menon
- Kitija Kuduma
