+++
title = "Digital Fabrication 2021"
+++

## Group Assignments

{{<row>}}
{{<col>}}
{{<card
  imgsrc="group-assignments/computer-controlled-cutting/thumbnail-1280x720.jpg"
  imgalt="Placeholder"
  title="Computer-Controlled Cutting"
  href="group-assignments/computer-controlled-cutting"
  hreftext="Show Documentation">}}
{{</col>}}

{{<col>}}
{{<card
  imgsrc="group-assignments/electronics-production/thumbnail-1280x720.jpg"
  imgalt="Placeholder"
  title="Electronics Production"
  href="group-assignments/electronics-production"
  hreftext="Show Documentation">}}
{{</col>}}

{{<col>}}
{{<card
  imgsrc="group-assignments/3d-scanning-and-printing/thumbnail-1280x720.jpg"
  imgalt="Placeholder"
  title="3D Scanning and Printing"
  href="group-assignments/3d-scanning-and-printing"
  hreftext="Show Documentation">}}
{{</col>}}
{{</row>}}

## Fab Academy Students

1. [issues](https://gitlab.fabcloud.org/academany/fabacademy/2019/labs/aalto/students/solomon-embafrash/-/issues),
  [www](http://fabacademy.org/2019/labs/aalto/students/solomon-embafrash/),
  Solomon Embafrash
1. [issues](https://gitlab.fabcloud.org/academany/fabacademy/2020/labs/kochi/students/ranjit-menon/-/issues),
  [www](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/),
  Ranjit Menon
1. [issues](https://gitlab.fabcloud.org/academany/fabacademy/2021/labs/aalto/students/kitija-kuduma/-/issues),
  [www](http://fabacademy.org/2021/labs/aalto/students/kitija-kuduma/),
  Kitija Kuduma

## Aalto Students

1. [issues](https://gitlab.com/dinany/fab-academy/-/issues),
  [www](https://dinany.gitlab.io/fab-academy/),
  Yan Dinan
1. [issues](https://gitlab.com/ONNI-INNOvations/fab-academy/-/issues),
  [www](https://onni-innovations.gitlab.io/fab-academy/),
  Eriksson Onni Oscar
1. [issues](https://gitlab.com/daali98/danielfablab2021/-/issues),
  [www](https://daali98.gitlab.io/danielfablab2021/),
  Wilenius Daniel Elias
1. [issues](https://gitlab.com/karlpalo/fablab2021/-/issues),
  [www](https://karlpalo.gitlab.io/fablab2021/),
  Karl Mihels
1. [issues](https://gitlab.fabcloud.org/academany/fabacademy/2021/labs/aalto/students/russianwu/-/issues),
  [www](http://fabacademy.org/2021/labs/aalto/students/russianwu/gikl/),
  Wu Ruo-Xuan

## Not Continuing

1. [issues](https://gitlab.com/MensahDann/fab-academy/-/issues),
  [www](https://mensahdann.gitlab.io/fab-academy/),
  Mensah Dann Kwami Mads
1. [issues](https://gitlab.com/dhop1/fab-academy/-/issues),
  [www](https://dhop1.gitlab.io/fab-academy/),
  Hopkins Daniel Evan
1. [issues](https://gitlab.com/ikola/fab-academy/-/issues),
   [www](https://ikola.gitlab.io/fab-academy/),
   Ikola Hannu Johannes
1. [issues](https://gitlab.com/eeropic/fablab-doc/-/issues),
   [www](https://eeropic.gitlab.io/fablab-doc/),
   Pitkänen Eero Johannes
